import { Request, Response } from 'express';
import Cookies = require('cookies');
import { api } from '../api';
import * as moment from 'moment';

const onboardCookieKey = 'userOnboarded';

/**
 * GET /
 * Home page.
 * Uses the Cookies module to detect
 * new users.
 *
 */
export const index = (req: Request, res: Response) => {
  const userOnboarded = getCookie(req, res);

  if (!userOnboarded) {
    res.redirect('onboarding');
    return;
  }
  api
    .getCampaigns()
    .then(campaignsFromServer => {
      const now = moment();

      // only display campaigns with enddate later than now
      const campaigns = campaignsFromServer.filter(campaign =>
        now.isBefore(moment(campaign.endDate))
      );
      res.render('campaigns', {
        title: 'Emergencies',
        moment,
        campaigns
      });
    })
    .catch((err: Error) => {
      console.log(err);
      res.sendStatus(500);
    });
};

export const onboardOne = (req: Request, res: Response) => {
  res.render('onboarding/onboarding1', {
    title: 'Welcome'
  });
};

export const onboardTwo = (req: Request, res: Response) => {
  res.render('onboarding/onboarding2', {
    title: 'Welcome'
  });
};

export const onboardThree = (req: Request, res: Response) => {
  putCookie(req, res, onboardCookieKey);

  res.render('onboarding/onboarding3', {
    title: 'Welcome'
  });
};

const getCookie = (req: Request, res: Response) => {
  const cookies = new Cookies(req, res);
  return cookies.get(onboardCookieKey);
};

const putCookie = (req: Request, res: Response, userKey: string) => {
  const cookies = new Cookies(req, res);
  // set the cookie in the browser with a max age of 20 years.
  cookies.set(userKey, 'true', { maxAge: 631138520000 });
};

export const healthy = (req: Request, res: Response) => {
  res.send(200);
};
