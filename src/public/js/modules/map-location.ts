import 'leaflet/dist/leaflet.css'; // tslint:disable-line
import * as leaflet from 'leaflet';

const init = (zoom: number, name: string, point: any) => {
  const map = leaflet.map(name).setView([point.lat, point.lng], zoom);

  leaflet
    .tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
      attribution:
        '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
    })
    .addTo(map);

  leaflet
    .circle([point.lat, point.lng], {
      color: 'red',
      fillColor: '#f03',
      fillOpacity: 0.5,
      radius: point.radius
    })
    .addTo(map);
};

(window as any).map = init;
