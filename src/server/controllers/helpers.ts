function getAbsoluteUrl(path: string) {
  const fixedPath = path[0] !== '/' ? `/${path}` : path;
  return encodeURI(`${process.env.OR_SITE}${fixedPath}`);
}

export { getAbsoluteUrl };
