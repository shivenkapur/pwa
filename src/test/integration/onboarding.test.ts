import { launch } from 'puppeteer';

import { app } from '../../server/app';

let server: any;

const appPort = app.get('port');

beforeAll(() => {
  server = app.listen(appPort, () => {
    console.log(
      '  App is running at http://localhost:%d in %s mode',
      appPort,
      app.get('env')
    );
    console.log('  Press CTRL-C to stop\n');
  });
});

afterAll(() => {
  server.close();
});

describe('access the onboarding page', () => {
  it(
    'should load the onboarding screen',
    done => {
      (async () => {
        const browser = await launch({ args: ['--no-sandbox'] });
        const page = await browser.newPage();

        await page.goto(`http://localhost:${appPort}`);

        await page.waitForSelector('#first.onboarding');
        await page.click('.row-buttons');

        await page.waitForSelector('#second.onboarding');
        await page.click('.row-buttons');

        await page.waitForSelector('#third.onboarding');
        await page.click('.row-buttons');

        await page.goto(`http://localhost:${appPort}/`);

        expect(await page.$('div.campaigns-card')).not.toBeNull();
        await browser.close();
        done();
      })();
    },
    15000
  );
});
