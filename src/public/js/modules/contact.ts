import { fetchLoader } from './onerelief';

const showSendSuccessMessage = () => {
  (document.getElementById('success-trigger') as HTMLInputElement).click();
};

const showMappedErrorMessages = (json: any) => {
  Object.keys(json.errors).forEach((key: any) => {
    getErrorMessageContainer().innerHTML += `<p>${json.errors[key].msg}</p>`;
  });
};

const showGeneralErrorMessage = () => {
  getErrorMessageContainer().innerHTML =
    'A problem occurred while sending your message. Please try again.';
};

const removeErrorMessage = () => {
  getErrorMessageContainer().innerHTML = '';
};

const getErrorMessageContainer = () => {
  return document.querySelector('#errors') as HTMLFormElement;
};

const sendMessage = () => {
  const form = document.querySelector('#contact-form') as HTMLFormElement;

  fetchLoader('/contact', {
    loader: 'send',
    method: 'POST',
    body: new FormData(form)
  })
    .then(response => {
      if (response.ok) {
        showSendSuccessMessage();
        return;
      }
      if (response.status === 400) {
        return response.json();
      }
      showGeneralErrorMessage();
    })
    .then(json => {
      if (json) {
        showMappedErrorMessages(json);
      }
    })
    .catch(e => {
      // Network or JSON parsing errors
      console.error(e);
      showGeneralErrorMessage();
    });
};

document.querySelector('#contact-form').addEventListener('submit', e => {
  e.preventDefault();
  removeErrorMessage();
  sendMessage();
});
