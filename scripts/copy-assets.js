const shell = require('shelljs');
const fs = require('fs');
shell.cp('-R', 'src/views', 'dist/');
shell.cp('-R', 'src/public/.well-known', 'dist/public/.well-known');
shell.cp('-R', 'src/public/!(css|js)', 'dist/public');
// ignore browserify symlink
shell.cp('-R', 'src/public/js/!(browserify)', 'dist/public/js');
