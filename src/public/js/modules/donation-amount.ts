import * as validate from 'validate.js';
import { localAmt } from './donation';
import { showLoader, hideLoader } from './loader';

const inputCustom = () =>
  document.getElementById('customAmount') as HTMLInputElement;

const showMessage = (mess: string) => {
  const div = document.getElementsByClassName('validation-message')[0];
  div.innerHTML = mess;
};

const getChecks = () =>
  Array.from(
    document
      .getElementsByClassName('fixed-amounts')[0]
      .getElementsByTagName('input')
  );

const getCustomAmount = () => {
  return inputCustom().value.trim();
};

const setCustomAmount = (amt: number) => {
  inputCustom().value = String(amt);
};

const clearCustomAmount = () => {
  inputCustom().value = '';
};

const fixedSelected = (checkbox: HTMLInputElement) => {
  getChecks().forEach(el => {
    if (el !== checkbox) {
      el.checked = false;
    }
  });
  if (checkbox.checked) {
    clearCustomAmount();
  }
};

const getFixedAmount = () => {
  let amt;
  getChecks().forEach(el => {
    if (el.checked) {
      amt = el.dataset.amount;
    }
  });
  if (!amt) {
    throw Error('amount not checked');
  }
  return amt;
};

const validateStoreAmount = () => {
  let fixed = false;
  getChecks().forEach(el => {
    if (el.checked) {
      fixed = true;
    }
  });
  const customAmount = getCustomAmount();
  if (!fixed && !customAmount) {
    showMessage('Please provide an amount');
    return false;
  }
  if (fixed && customAmount) {
    showMessage('Please provide a single amount');
    return false;
  }
  // If input type number is respected then this is only to check for
  // decimal numbers.
  if (
    customAmount &&
    validate.single(customAmount, { numericality: { onlyInteger: true } })
  ) {
    showMessage('Amount is not a whole number');
    return false;
  }
  if (fixed) {
    localAmt(parseInt(getFixedAmount(), 10));
  } else {
    localAmt(parseInt(customAmount, 10));
  }
  return true;
};

const init = () => {
  getChecks().forEach(el => {
    el.checked = false;
  });
  const amount = localAmt();
  if (amount) {
    setCustomAmount(amount);
  }
};

function showSpinner() {
  showLoader('continue');
}

function hideSpinner() {
  hideLoader('continue');
}

(window as any).donationAmount = {
  init,
  showSpinner,
  hideSpinner,
  fixedSelected,
  validateStoreAmount
};
