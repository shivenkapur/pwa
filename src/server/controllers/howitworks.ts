import { Request, Response } from 'express';

/**
 * GET /
 * How OneRelief Works page.
 */
export let index = (req: Request, res: Response) => {
  res.render('howitworks', {
    title: 'How OneRelief Works'
  });
};
