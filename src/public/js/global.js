if (window.env !== 'production') {
  window.debug = console.log;
} else {
  window.debug = mess => {};
}
