import * as nodemailer from 'nodemailer';
import * as aws from 'aws-sdk';
// tslint:disable-next-line
import { SmtpOptions } from 'nodemailer-smtp-transport';

// fix-up lack of predefined service fields on TS definition
declare interface SESSmtpOptions extends SmtpOptions {
  SES: aws.SES;
}

type EmailService = 'mailgun' | 'ses';

const service: EmailService = process.env.OR_EMAIL_SERVICE as EmailService;

let transporter: nodemailer.Transporter;

switch (service) {
  case 'mailgun':
    transporter = nodemailer.createTransport({
      service: 'Mailgun',
      auth: {
        user: process.env.OR_SMTP_USER,
        pass: process.env.OR_SMTP_PASSWORD
      }
    });
    break;
  case 'ses':
    const awsOptions: SESSmtpOptions = {
      SES: new aws.SES({
        apiVersion: '2010-12-01'
      })
    };
    transporter = nodemailer.createTransport(awsOptions);
    break;
  default:
    throw Error('invalid OR_EMAIL_SERVICE');
}

const sendEmail = (mailProps: nodemailer.SendMailOptions) => {
  return new Promise<nodemailer.SentMessageInfo>((resolve, reject) => {
    transporter.sendMail(
      mailProps,
      (error: Error, info: nodemailer.SentMessageInfo) => {
        if (error) {
          reject(error);
          return;
        }
        resolve(info);
      }
    );
  });
};

export { sendEmail };
