declare interface Charity {
  id: string;
  name: string;
  description: string;
  logo: Photo;
  updatedAt: Date;
  createdAt: Date;
}
