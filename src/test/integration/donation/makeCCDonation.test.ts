import { ElementHandle, Page, Frame, launch } from 'puppeteer';

import { app } from '../../../server/app';

let server: any;

const appPort = app.get('port');

beforeAll(() => {
  return new Promise((resolve, fail) => {
    server = app.listen(appPort, () => {
      console.log(
        '  App is running at http://localhost:%d in %s mode',
        appPort,
        app.get('env')
      );
      console.log('  Press CTRL-C to stop\n');
      resolve(server);
    });
  });
});

afterAll(() => {
  server.close();
});

const getFrame = (page: Page, name: string): Frame => {
  let frame;
  page.frames().forEach((fr: Frame) => {
    if (fr.name().indexOf(name) > -1) {
      frame = fr;
    }
  });
  return frame;
};

describe('make a cc payment', () => {
  it(
    'should submit a transaction to Braintree',
    done => {
      (async () => {
        const campaignId = '1';

        const browser = await launch({ args: ['--no-sandbox'] });
        const page = await browser.newPage();
        await page.goto(
          `http://localhost:${appPort}/campaigns/${campaignId}/donation`
        );
        await page.waitForSelector('#continue');

        await page.click('#fixedOne');
        await page.click('#continue');

        await page.waitForSelector('#submit-button', { visible: true });

        const ccNumSel = '#credit-card-number';
        const ccFrame = getFrame(page, 'braintree-hosted-field-number');

        await ccFrame.$(ccNumSel).then((el: ElementHandle) => {
          return el.type('4111111111111111');
        });

        const cvvFrame = getFrame(page, 'braintree-hosted-field-cvv');
        await cvvFrame.$('#cvv').then((el: ElementHandle) => el.type('123'));

        const expFrame = getFrame(
          page,
          'braintree-hosted-field-expirationDate'
        );
        await expFrame
          .$('#expiration')
          .then((el: ElementHandle) => el.type('0130'));

        await page.type('input#fullname', 'Puppeteer');
        await page.type('input#email', 'puppeteer@google.com');

        await page.click('#submit-button');
        await page.waitForSelector('#finish');
        await browser.close();
        done();
      })();
    },
    15000
  );
});
