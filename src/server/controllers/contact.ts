import { Request, Response, NextFunction, Application } from 'express';
import * as multer from 'multer';
// tslint:disable-next-line:no-submodule-imports
import { check, validationResult } from 'express-validator/check';

import { sendEmail } from '../modules/email';

const routes = (app: Application) => {
  const upload = multer();

  app.get('/contact', index);
  app.post(
    '/contact',
    [
      upload.array(null),
      check('name')
        .not()
        .isEmpty()
        .withMessage('Please supply a name.'),
      check('em')
        .isEmail()
        .withMessage('Please supply a valid email address.'),
      check('message')
        .not()
        .isEmpty()
        .withMessage('Please supply a message.')
    ],
    [
      (req: Request, res: Response, next: NextFunction) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
          return res.status(400).json({ errors: errors.mapped() });
        }
        return submit(req, res);
      }
    ]
  );
};

/**
 * GET /
 * Contact Us page.
 */
const index = (req: Request, res: Response) => {
  res.render('contact', {
    title: 'Contact Us'
  });
};

/**
 * POST /
 * Contact Us page.
 */
const submit = (req: Request, res: Response) => {
  const honeypotEmailValue = req.body.email;

  if (honeypotEmailValue) {
    console.warn(
      `Honeypot email value '${honeypotEmailValue}' filled in - ignoring request and returning 'accepted'`
    );
    res.sendStatus(201);
    return;
  }

  const mailOptions = {
    replyTo: req.body.em,
    from: process.env.OR_CONTACT_EMAIL,
    to: process.env.OR_CONTACT_EMAIL,
    subject: `Contact Form submission from ${req.body.em}`,
    text: req.body.message,
    html: req.body.message
  };

  sendEmail(mailOptions)
    .then(info => {
      console.debug(`Message sent: ${info.response} - returning 'accepted'`);
      res.sendStatus(201);
    })
    .catch(err => {
      console.error(err);
      res.sendStatus(500);
    });
};

export { index, submit, routes };
