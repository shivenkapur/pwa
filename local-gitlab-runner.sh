#!/bin/bash

# requires local installation of Docker and gitlab-runner

# env props must not have spaces in the values for gitlab-runner command line to parse correctly!

# remove empty lines and comments
ENV_VARS=$(cat .env | sed -e '/^\s*$/d' | grep -v ^# | sed -e 's/^/--env /')
echo "${ENV_VARS}"

# export "$(cat .env | grep -v ^# | xargs)" & 
gitlab-runner exec docker $(echo "$ENV_VARS" | xargs -0 ) --docker-privileged -docker-volumes /var/run/docker.sock:/var/run/docker.sock --docker-image "docker:stable" $1

