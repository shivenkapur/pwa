declare interface Photo {
  prototype: Blob;
  id: string;
  donationId: string;
  fileName: string;
  url: string;
  updatedAt: Date;
  createdAt: Date;
}
